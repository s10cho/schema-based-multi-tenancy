package sycho.examples.multitenancy.service;

import org.springframework.stereotype.Service;
import sycho.examples.multitenancy.entity.Note;
import sycho.examples.multitenancy.repository.NoteRepository;

@Service
public class NoteService {

    private NoteRepository repository;

    public NoteService(NoteRepository repository) {
        this.repository = repository;
    }

    public Note createNote(Note note) {
        return repository.save(note);
    }

    public Note findNote(Long id) {
        return repository.findById(id).orElseThrow();
    }

    public Iterable<Note> findAllNotes() {
        return repository.findAll();
    }
}
