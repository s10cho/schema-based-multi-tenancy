CREATE TABLE t_user
(
    id       INT AUTO_INCREMENT,
    username VARCHAR(255) UNIQUE,
    password VARCHAR(255)
);

CREATE TABLE t_public_user
(
    id       INT,
    username VARCHAR(255)
);